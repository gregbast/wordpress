<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package corporato
 * @since corporato 1.0
 * @since corporato 1.0
 */

get_header();
?>
	<div class="container no-page-404">
		<div class="col-12">
			<h1 class="page-title"><?php esc_html_e( '404', 'corprato' ); ?></h1>
			<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'corprato' ); ?></p>
			<br>
			<?php get_search_form(); ?>
			<br>
			<?php esc_html__('Or you can go to homepage', 'corprato'); ?>
			<br>
			<a href="<?php echo esc_url(home_url()); ?>" class="btn-2"><?php esc_html_e('home', 'corprato'); ?></a>
		</div> <!-- col-12 -->
	</div><!-- .container -->

<?php
get_footer();
