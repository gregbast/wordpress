<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package corporato
 * @since corporato 1.0
 * @since corporato 1.0
 */

get_header();

$description = get_the_archive_description();
?>
<div class="container">
	<div class="col-8">
		<?php if ( have_posts() ) : ?>
		<header class="page-header alignwide">
			<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
			<?php if ( $description ) : ?>
				<div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
			<?php endif; ?>
		</header><!-- .page-header -->
		<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
			<?php get_template_part( 'template-parts/content/content'); ?>
		<?php endwhile; ?>
		<?php the_posts_pagination(array(
			'prev_text' => esc_html__('prv', 'corprato'),
			'next_text' => esc_html__('next', 'corprato'),
			'screen_reader_text' => ' ',
		)); ?>
		<?php else : ?>
		<?php get_template_part( 'template-parts/content/content-none' ); ?>
		<?php endif; ?>
	</div>
	<div class="col-4">
		<?php dynamic_sidebar('right_sidebar'); ?>
	</div>
</div>

<?php get_footer(); ?>
