(function($){
    $(window).on('load', function(){
        $('.preloaderfullwrapper').fadeOut(500);
    });

    $(document).ready(function(){
        $('.menu-toggle-hide').click(function(){
            $('.main-navigation').removeClass('toggled');
            $('body').removeClass('menu-opened');
        });
    });
})(jQuery);