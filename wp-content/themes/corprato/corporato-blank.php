<?php
/**
 * The template for displaying the blank page
 *
 * Contains the blank page with only the header and footer. And you can display anything by shortcode or elementor page builder here.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package corporato
 * @since corporato 1.0
 */
/*
Template Name: Corporato Blank
*/
get_header();
the_content();
get_footer();
?>