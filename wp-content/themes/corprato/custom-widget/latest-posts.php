<?php
namespace Elementor;

class corprato_latest_post extends Widget_Base {
	
	public function get_name() {
		return 'latest-posts';
	}
	
	public function get_title() {
		return 'Latest Posts';
	}
	
	public function get_icon() {
		return 'fa fa-pen';
	}
	
	public function get_categories() {
		return [ 'basic' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => esc_html__( 'Latest Posts', 'corprato' ),
			]
		);
		
		$this->add_control(
			'title',
			[
				'label' => esc_html__( 'Post Quantity to show', 'corprato' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter How many posts you want to shwo here', 'corprato' ),
			]
		);

		$this->end_controls_section();
	}
	
	protected function render() {

        $settings = $this->get_settings_for_display();

        $latestblogpost = new \WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => $settings['title'],
        ));
        while($latestblogpost->have_posts()) : $latestblogpost->the_post () ?>
            <div class="blogwrapper">
                <div class="blogthumb">
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                </div> <!-- blogthumb -->
                <div class="blogcontent">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="blogdetails">
                        <?php _e('Posted by', 'corprato') ?> <strong><?php the_author(); ?></strong>, <?php _e('AT', 'corprato'); ?> <i><?php the_time('d-M-Y') ?></i> <?php _e('and has got', 'corprato') ?>
						<?php 
						$zero_cmnt = __('0 Comment', 'corprato');
						$one_cmnt = __('1 Comment', 'corprato');
						$more_cmnts = __('% Comment', 'corprato');
						$disable_cmnt = __('Comment Disbale', 'corprato');
						
						comments_popup_link($zero_cmnt, $one_cmnt, $more_cmnts, 'text-decoration-none base-color', $disable_cmnt); ?>
                        <?php _e('Category of', 'corprato'); ?> <?php the_category(', '); ?>
                    </div> <!-- blogdetails -->
                    <p><?php echo wp_trim_words(get_the_content(), 40, '....'); ?> <a href="<?php the_permalink(); ?>" class="continue-reading"><?php _e('continue reading', 'corprato'); ?></a></p>
                </div> <!-- blogcontent -->
            </div> <!-- blogwrapper -->
            <?php endwhile;
		 

	}
	
	protected function _content_template() {

    }
	
	
}