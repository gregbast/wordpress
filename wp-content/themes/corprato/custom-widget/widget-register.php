<?php

class corprato_Widgets {

	protected static $instance = null;

	public static function get_instance() {
		if ( ! isset( static::$instance ) ) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	protected function __construct() {
		include_once('latest-posts.php');
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'register_widgets' ] );
	}

	public function register_widgets() {
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\corprato_latest_post() );
	}

}

add_action( 'init', 'corprato_init' );

function corprato_init() {
	corprato_Widgets::get_instance();
}