<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package corporato
 * @since corporato 1.0
 */

?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->
	<div class="container">
		<div class="footer-widget-full">
			<?php dynamic_sidebar( 'footer_siedbar' ); ?>
		</div> <!-- footer-widget-full -->
	</div>

	<footer id="colophon" class="site-footer container" role="contentinfo">
		<div class="col-8">
		<p><?php echo esc_html(get_theme_mod('corprato_footer_copyright')); ?></p>
		</div>
		<div class="col-4 text-right">
			<a href="#"><?php esc_html__('back to top', 'corprato'); ?></a>
		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
