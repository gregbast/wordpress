<?php
function corprato_setup() {

	//Textdomain
	load_theme_textdomain( 'corprato', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*title-tag*/
	add_theme_support( 'title-tag' );

	/*Add post-formats support.*/
	$post_formate = array(
		'link',
		'aside',
		'gallery',
		'image',
		'quote',
		'status',
		'video',
		'audio',
		'chat',
	);
	add_theme_support('post-formats', $post_formate);

	/*CUSTOM BACKGROUND*/
	$arg_custom_bg = array('default-color' => 'd1e4dd');
	add_theme_support('custom-background', $arg_custom_bg);

	/*post-thumbnail*/
	add_theme_support( 'post-thumbnails' );
	
	/*Menubar Register*/
	register_nav_menus(
		array(
			'primary' => esc_html__( 'Main menu', 'corprato' ),
		)
	);

	/*HTML5*/
	$html_args = array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		'style',
		'script',
		'navigation-widgets',
	);
	add_theme_support('html5', $html_args);

	/*custom logo*/
	$custom_logo_args = array(
		'flex-width'           => true,
		'flex-height'          => true,
		'unlink-homepage-logo' => false,
	);
	add_theme_support('custom-logo', $custom_logo_args);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Add support for Block Styles.
	add_theme_support( 'wp-block-styles' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for editor styles.
	add_theme_support( 'editor-styles' );

	$editor_stylesheet_path = get_template_directory(). '/assets/css/style-editor.css';

	// Enqueue editor styles.
	add_editor_style( $editor_stylesheet_path );

	/*Responsive Embed*/
	add_theme_support( 'responsive-embeds' );
}
add_action( 'after_setup_theme', 'corprato_setup' );

/**
 * Register widget area.
 *
 * @since corprato 1.0
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 *
 * @return void
 */
function corprato_widgets_init() {

	register_sidebar(
		array(
			'name'          => esc_html__( 'Right Sidebar', 'corprato' ),
			'id'            => 'right_sidebar',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'corprato' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s widgetwrapper">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer', 'corprato' ),
			'id'            => 'footer_siedbar',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'corprato' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s widgetwrapper">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'corprato_widgets_init' );

/**
 * Enqueue scripts and styles.
 *
 * @since corprato 1.0
 *
 * @return void
 */
function corprato_scripts() {
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'corprato-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );

	//corprato styles
	wp_enqueue_style( 'corprato-css', get_template_directory_uri() . '/assets/css/style.css', array(), wp_get_theme()->get( 'Version' ) );

	// Threaded comment reply styles.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Main navigation scripts.
	wp_enqueue_script('corprato-primary-navigation-script', get_template_directory_uri() . '/assets/js/primary-navigation.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true);

	// Responsive embeds script.
	wp_enqueue_script('corprato-responsive-embeds-script', get_template_directory_uri() . '/assets/js/responsive-embeds.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true);

	//main jquery
	wp_enqueue_script('corprato-mainjs', get_template_directory_uri().'/assets/js/main.js', array('jquery'), false);
}
add_action( 'wp_enqueue_scripts', 'corprato_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @since corprato 1.0
 *
 * @link https://git.io/vWdr2
 */
function corprato_skip_link_focus_fix() {

	// If SCRIPT_DEBUG is defined and true, print the unminified file.
	if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
		echo '<script>';
		include get_template_directory() . '/assets/js/skip-link-focus-fix.js';
		echo '</script>';
	}

	// The following is minified via `npx terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'corprato_skip_link_focus_fix' );

/**
 * Enqueue scripts for the customizer.
 *
 * @since corprato 1.0
 *
 * @return void
 */
function corprato_customize_preview_init() {
	wp_enqueue_script('corprato-customize-js', get_theme_file_uri( '/assets/js/customize.js' ), array(), wp_get_theme()->get( 'Version' ), true);
}
add_action( 'customize_preview_init', 'corprato_customize_preview_init' );


// Adding the Elementor Widget
if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
}
if (is_plugin_active( 'elementor/elementor.php' )) {
	require_once('custom-widget/widget-register.php');
}

//Adding TGMPA
require_once 'plugin-required/class-tgm-plugin-activation.php';
require_once 'plugin-required/activations.php';

//Customizer
//Footer Copyright text
function corprato_footercopyright($corprato_footercopyright){
	$corprato_footercopyright->add_section('footer_area', array(
		'title' => esc_html__('Footer', 'corprato'),
		'priority' => 50,
	));
	$corprato_footercopyright->add_setting('corprato_footer_copyright', array(
		'default' => esc_html__('&copy; 2021 All right reserved', 'corprato'),
		'transport' => 'refresh',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$corprato_footercopyright->add_control('corprato_footer_copyright', array(
		'section' => 'footer_area',
		'label' => esc_html__('Footer copyright', 'corprato'),
		'type' => 'text',
	));
}
add_action('customize_register', 'corprato_footercopyright');