<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package corporato
 * @since corporato 1.0
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="preloaderfullwrapper">
	<div class="prelaoder">
		<h3 class="loader"><?php echo esc_html( get_bloginfo( 'name' ) ); ?><span class="one"></span><span class="two"></span><span class="three"></span><span class="four"></span></h3>
	</div> <!-- prelaoder -->
</div> <!-- preloaderfullwrapper -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'corprato' ); ?></a>
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php if ( has_custom_logo() ) : ?>
				<div class="site-logo"><?php the_custom_logo(); ?></div>
			<?php endif; ?>
			<?php if ( !has_custom_logo() ) : ?>
				<a href="<?php echo esc_url(home_url()); ?>" class="site-title"><strong><?php echo bloginfo('name'); ?></strong></a>
			<?php endif; ?>
		</div><!-- .site-branding -->
		<?php if ( has_nav_menu( 'primary' ) ) : ?>
			<nav id="site-navigation" class="main-navigation">
				<button id="nav-icon" class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</button>
				<!-- main-menu -->
				<div class="main-menu">
					<?php
					if ( has_nav_menu( 'primary' ) ) {
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
							)
						);
					}
					?>
					<button type="button" class="menu-toggle-hide">
						<span class="dashicons dashicons-no-alt"></span> <?php esc_html_e('close', 'corprato'); ?>
					</button>
				</div>
			</nav><!-- #site-navigation -->
		<?php endif; ?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
