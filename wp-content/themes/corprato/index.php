<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package corporato
 * @since corprato 1.0
 */

get_header(); ?>
<div class="container">
	<div class="col-8">
		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
				the_post();

				get_template_part( 'template-parts/content/content');
			}

			// Previous/next page navigation.
			wp_link_pages(
				array(
					'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'corprato' ) . '">',
					'after'    => '</nav>',
					/* translators: %: Page number. */
					'pagelink' => esc_html__( 'Page %', 'corprato' ),
				)
			);

		} else {

			// If no content, include the "No posts found" template.
			get_template_part( 'template-parts/content/content-none' );

		}
		the_posts_pagination(array(
			'prev_text' => esc_html__('prv', 'corprato'),
			'next_text' => esc_html__('next', 'corprato'),
			'screen_reader_text' => ' ',
		));
		?>
	</div>
	<div class="col-4">
		<?php dynamic_sidebar('right_sidebar'); ?>
	</div>
</div>
<?php get_footer();