<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package corporato
 * @since corporato 1.0
 */

get_header(); ?>

<div class="container">
	<div class="col-8">
		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content/content-page' );

			// If comments are open or there is at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		endwhile; ?>
	</div>
	<div class="col-4">
		<?php dynamic_sidebar('right_sidebar'); ?>
	</div>
</div>

<?php get_footer();
