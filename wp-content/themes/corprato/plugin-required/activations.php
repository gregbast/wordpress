<?php

add_action( 'tgmpa_register', 'corprato_register_required_plugins' );
function corprato_register_required_plugins() {
	$plugins = array(
		array(
			'name'        => 'Elementor Website Builder',
			'slug'        => 'elementor',
			'is_callable' => 'wpseo_init',
		),
		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
		),
		array(
			'name'               => 'Contact Form 7',
			'slug'               => 'contact-form-7',
			'is_callable' => 'wpseo_init',
		),

	);
	$config = array(
		'id'           => 'corprato',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
