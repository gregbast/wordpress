=== Corporato ===
Contributors: atuljindal
Requires at least: 5.3
Tested up to: 5.9
Requires PHP: 5.6
Stable tag: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Description: corprato is a blank canvas for your open minded ideas and it makes the block editor your best brush. With block patterns, which allow you to create a beautiful layout quickly, this theme’s soft unique and attracting colors and design will make your work shine. So, why not you take this as your theme to make your portfolio, business website, or personal blog. And it's created by <a href="https://convertrank.com/">convertrank.com</a>

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Corporato in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to INSERT ABOUT PAGE for a guide on how to customize this theme.
5. Navigate to Appearance > Customize in your admin panel and customize to your taste.

== Privacy ==
No data is saved in the database or transferred about an user.

== Changelog ==
= 1.0.0 =
Initial release

= 2.0.0 =
Some changes made in functions and fix page error

= 3.0.0 =
Menu close button added and some coyright issues changes and default logo added

= 4.0.0 =
Modal remove from menu, logo changed and copyright license/credit issue fixed, unique prefix added for all functions, global classes and so on.

= 5.0.0 =
Screenshot.png size compressed, wordpress version update

= 5.0.1 =
Theme developers link added

= 5.0.2 =
Theme developers link added to introduce them with users

= credits =
Image for theme screenshot, Copyright Jason Blackeye
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/4B83RD7BV9

TGM Plugin Activation
Copyright (c) 2022 Thomas Griffin & GaryJones
License: GPL-2.0 License, https://opensource.org/licenses/GPL-2.0
Source: http://tgmpluginactivation.com/

== Copyright ==
Corporato WordPress Theme, 2021-2022 convertrank.com
Corporato is distributed under the terms of the GNU GPLv2 or later.