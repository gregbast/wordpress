<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package corporato
 * @since corprato 1.0
 */

get_header(); ?>

<div class="container">
	<div class="col-8">
	<?php
	if ( have_posts() ) {
		?>
		<header>
			<h1>
				<?php
					printf(
						/* translators: %s: Search term. */
						esc_html__( 'Results for "%s"', 'corprato' ),
						'<span class="page-description search-term">' . esc_html( get_search_query() ) . '</span>'
					);
					printf(
					esc_html(
						/* translators: %d: The number of search results. */
						_n(
							'and We found %d result for your search.',
							'and We found %d results for your search.',
							(int) $wp_query->found_posts,
							'corprato'
						)
					),
					(int) $wp_query->found_posts
					);
				?>
			</h1>
		</header>
		<?php
		// Start the Loop.
		while ( have_posts() ) {
			the_post();
			get_template_part( 'template-parts/content/content');
		}

		// Previous/next page navigation.
		the_posts_pagination(array(
			'prev_text' => esc_html__( 'Prv', 'corprato' ),
			'next_text' => esc_html__( 'Next', 'corprato' ),
			'screen_reader_text' => ' ',
		));

		// If no content, include the "No posts found" template.
	} else {
		get_template_part( 'template-parts/content/content-none' );
	}
	?>
	</div> <!-- col-8 -->
	<div class="col-4">
		<?php dynamic_sidebar('right_sidebar'); ?>
	</div> <!-- col-4 -->
</div>


<?php
get_footer();
