<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php echo esc_attr( 'Search', 'corprato' ); ?>"/>
	<button type="submit" id="searchsubmit"><span class="dashicons dashicons-search"></span></button>
</form>