<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package corporato
 * @since corporato 1.0
 */

get_header(); ?>
<div class="container">
	<div class="col-8">
		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content/content-single' );
			//Posts next/prv navigation
			$corporato_next_label     = esc_html__( 'Next post', 'corprato' );
			$corporato_previous_label = esc_html__( 'Previous post', 'corprato' );

			the_post_navigation(
				array(
					'next_text' => '<p class="meta-nav">' .$corporato_next_label. ' <span class="dashicons dashicons-arrow-right-alt"></span></p><p class="post-title">%title</p>',
					'prev_text' => '<p class="meta-nav"><span class="dashicons dashicons-arrow-left-alt"></span> ' .$corporato_previous_label. '</p><p class="post-title">%title</p>',
				)
		);
			// If comments are open or there is at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		endwhile; // End of the loop.
		?>
	</div> <!-- col-8 -->
	<div class="col-4">
		<?php dynamic_sidebar('right_sidebar'); ?>
	</div> <!-- col-4 -->
</div> <!-- container -->

<?php
get_footer();
