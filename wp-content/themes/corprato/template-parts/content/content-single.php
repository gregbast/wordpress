<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package corporato
 * @since corporato 1.0
 * @since corporato 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="single-blog-heading">
		<?php the_post_thumbnail(); ?>
		<h2 class="single-blog-post-header"><?php the_title(); ?></h2>
	</header><!-- .entry-header -->

	<div class="single-blog-footer">
		<div class="blogdetails">
			<?php _e('Posted by', 'corprato') ?> <strong><?php the_author(); ?></strong>, <?php _e('AT', 'corprato'); ?> <i><?php the_time('d-M-Y') ?></i> <?php _e('and has got', 'corprato') ?>
			<?php 
			$zero_cmnt = __('0 Comment', 'corprato');
			$one_cmnt = __('1 Comment', 'corprato');
			$more_cmnts = __('% Comment', 'corprato');
			$disable_cmnt = __('Comment Disbale', 'corprato');
			comments_popup_link($zero_cmnt, $one_cmnt, $more_cmnts, 'text-decoration-none base-color', $disable_cmnt); ?>
			<?php _e('On the Category of', 'corprato'); ?> <?php the_category(', '); ?> 
			<?php the_tags(); ?>
		</div> <!-- blogdetails -->
	</div><!-- .single-blog-footer -->

	<div class="single-blog-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'corprato' ) . '">',
				'after'    => '</nav>',
				/* translators: %: Page number. */
				'pagelink' => esc_html__( 'Page %', 'corprato' ),
			)
		);
		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
