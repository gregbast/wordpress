<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package corporato
 * @since corporato 1.0
 * @since corporato 1.0
 */

?>
<div id="post-<?php the_ID(); ?>" <?php post_class('blogwrapper'); ?>>
	<div class="blogthumb">
		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
	</div> <!-- blogthumb -->
	<div class="blogcontent">
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<div class="blogdetails">
			<?php _e('Posted by', 'corprato') ?> <strong><?php the_author(); ?></strong>, <?php _e('AT', 'corprato'); ?> <i><?php the_time('d-M-Y') ?></i> <?php 
			$zero_cmnt = __('0 Comment', 'corprato');
			$one_cmnt = __('1 Comment', 'corprato');
			$more_cmnts = __('% Comment', 'corprato');
			$disable_cmnt = __('Comment Disbale', 'corprato');
			comments_popup_link($zero_cmnt, $one_cmnt, $more_cmnts, 'text-decoration-none base-color', $disable_cmnt); ?>
			<?php the_category(); ?>
		</div> <!-- blogdetails -->
		<p><?php echo wp_trim_words(get_the_content(), 40, '....'); ?></p>
		<a href="<?php the_permalink(); ?>" class="btn-1"><?php _e('read more', 'corprato'); ?></a>
	</div> <!-- blogcontent -->
	<?php
	wp_link_pages(
		array(
			'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'corprato' ) . '">',
			'after'    => '</nav>',
			/* translators: %: Page number. */
			'pagelink' => esc_html__( 'Page %', 'corprato' ),
		)
	);
	?>
</div> <!-- blogwrapper -->
